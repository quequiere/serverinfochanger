﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Asphalt;
using Eco.Core.Plugins.Interfaces;
using Harmony;
using ServerInfoChanger.inject;

namespace ServerInfoChanger
{

    [AsphaltPlugin]
    public class ServerInfoChanger : IModKitPlugin
    {
        public static string prefix = "ServerInfoChanger: ";

        public void OnPostEnable()
        {
            printMessage("Plugin started.");

            Asphalt.Api.Asphalt.Harmony.Patch(NetworkInjector.TargetMethod(), null, new HarmonyMethod(typeof(NetworkInjector), "Postfix"));
            printMessage("Plugin loaded.");
        }

        public string GetStatus()
        {
            return "ServerInfoChanger started";
        }

        public static void printMessage(string message)
        {
            Console.WriteLine($"{prefix} {message}");
        }
    }
}
