﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Eco.Plugins.Networking;
using Eco.Shared.Networking;
using Eco.Shared.Utils;
using Harmony;

namespace ServerInfoChanger.inject
{
    //[HarmonyPatch]
    public class NetworkInjector
    {
        public static System.Reflection.MethodBase TargetMethod()
        {
            var method = typeof(NetworkManager).GetMethod("GetServerInfo", BindingFlags.Static | BindingFlags.Public);
            return method ;
        }

        public static void Postfix(ServerInfo __result)
        {
            int minOnline = 2;
            int onlineUped = (int)Math.Ceiling(__result.OnlinePlayers * 1.30);

            __result.OnlinePlayers = (int) Math.Max(minOnline, onlineUped);
            __result.TotalPlayers = __result.TotalPlayers + 20;
            __result.TimeSinceStart = __result.TimeSinceStart*0.60;

            __result.Description = getTittle();
            __result.DetailedDescription = getDescription();
        }

        public static string getTittle()
        {
            //[EU || FR] Eco-server.com [Giant World]
            List<string> tittles = new List<string>()
            {
                Text.ColorUnity(Color.ForestGreen.UInt,Text.Bold("Eco")),
                Text.Bold("-"),
                Text.ColorUnity(Color.Orange.UInt,Text.Bold("server")),
                Text.Bold("."),
                Text.ColorUnity(Color.Red.UInt,Text.Bold("com ")),

                Text.ColorUnity(Color.Yellow.UInt,Text.Bold("Skill X1.3 ")),
                Text.ColorUnity(Color.ForestGreen.UInt,Text.Bold("[5km Giant World] ")),
                Text.ColorUnity(Color.Cyan.UInt,Text.Italics("Beginners friendly")),
            };

            return string.Join("", tittles);
        }

        public static string getDescription()
        {

            List<string> desc = new List<string>()
            {
                "",
                Text.Size(1.7f,$"{Text.ColorUnity(Color.Red.UInt,"24/24")} & {Text.ColorUnity(Color.Blue.UInt,"7/7")} {Text.Bold("BIG Dedicated server without lags !")}"),
                Text.ColorUnity(Color.Yellow.UInt, Text.Size(1.5f,$"96 RAM 2XCPU NVME")),
                Text.ColorUnity(Color.Yellow.UInt, Text.Size(1.5f,$"Skills X 1.3")),
                Text.ColorUnity(Color.Green.UInt, Text.Size(1.5f,$"{Text.Size(1.5f,"Giant")} world ! {Text.Bold("5 km²")}")),
                "",
                "",
                Text.Underline(Text.Size(1.7f,"Custom light plugins:")),
                "",
                Text.ColorUnity(Color.Orange.UInt,Text.Size(1.5f,"NextFood")),
                "- See in advance the effect of food on your skills points",
                "- See the best food to buy on the market to max your skills points",
                "",
                Text.ColorUnity(Color.Orange.UInt,Text.Size(1.5f,"MoreConfig")),
                "- Tree growth time divide by 2",
                "- Tailings product divide by 3",
                Text.ColorUnity(Color.Orange.UInt,Text.Size(1.5f,"BetterLink")),
                "- The link between chest and stockpile has a greater distance",
                "",
                "",
                Text.Underline(Text.Size(1.7f,"Good to know")),
                Text.Size(1.3f,Text.ColorUnity(Color.Red.UInt,Text.Bold("No admin cheat"))),
                Text.Size(1.3f,Text.ColorUnity(Color.Cyan.UInt,"Big server with no lags")),
                Text.Size(1.3f,$"Server {Text.Bold("will be maintained")} till the end OR players requests"),
                Text.Size(1.3f,$"{Text.ColorUnity(Color.ForestGreen.UInt,"Admin is developer")} and listening user requests"),
                Text.Size(1.3f,"Recruiting server manager"),
                "",
                "",
                Text.Underline(Text.Size(1.7f,"Languages")),
                "- English",
                "- French",
                "",
                "",
                Text.Underline(Text.Size(1.7f,"Links")),
                $"Discord community: {Text.ColorUnity(Color.LightBlue.UInt,"https://discord.gg/hxtVcpm")}",
                $"Website:  {Text.ColorUnity(Color.LightBlue.UInt,"http://www.eco-server.com ")}",



            };

            return string.Join("\n", desc);
        }
    }
}
